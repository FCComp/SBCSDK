Pod::Spec.new do |s|


  s.name         = "SBCSDK"
  s.version      = "1.1.3"
  s.summary      = "SBCSDK s"
  s.description  = <<-DESC 
sobey任务管理系统
                   DESC

  s.homepage     = "https://gitee.com/FCComp/SBCSDK"
  # s.screenshots  = "www.example.com/screenshots_1.gif", "www.example.com/screenshots_2.gif"

  # s.license      = "MIT (example)"
  s.license      = { :type => "MIT", :file => "LICENSE" }

  s.author             = { "fczhouyou" => "zhouyou@sobey.com" }
  # Or just: s.author    = "ZhouYou"
  # s.authors            = { "ZhouYou" => "zhouyou@sobey.com" }
  # s.social_media_url   = "http://twitter.com/ZhouYou"

  # s.platform     = :ios
  s.platform     = :ios, "10.0"

  #  When using multiple platforms
  # s.ios.deployment_target = "5.0"
  # s.osx.deployment_target = "10.7"
  # s.watchos.deployment_target = "2.0"
  # s.tvos.deployment_target = "9.0"


  s.source       = { :git => "https://gitee.com/FCComp/SBCSDK.git", :tag => "#{s.version}" }
  s.source_files  = "SBCSDK/SBCSDK.framework/Headers/*.h"
  # s.exclude_files = "Classes/Exclude"
  # s.public_header_files = "Classes/**/*.h"

  # s.resource  = "SBCSDK/*.bundle"
  # s.resources = "Resources/*.png"
  # s.preserve_paths = "FilesToSave", "MoreFilesToSave"

  s.vendored_frameworks = 'SBCSDK/*.framework'
  # s.framework  = "SomeFramework"
  # s.frameworks = "SomeFramework", "AnotherFramework"

  # s.library   = "iconv"
  # s.libraries = "iconv", "xml2"


  # ――― Project Settings ――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #
  #
  #  If your library depends on compiler flags you can set them in the xcconfig hash
  #  where they will only apply to your library. If you depend on other Podspecs
  #  you can include multiple dependencies to ensure it works.

  # s.requires_arc = true

  # s.xcconfig = { "HEADER_SEARCH_PATHS" => "$(SDKROOT)/usr/include/libxml2" }
   #s.dependency "TMUserCenter"

s.dependency "AFNetworking" 
s.dependency "JPush" 

end
