//
//  SBCModuleModel.h
//  SBCSDK
//
//  Created by sobey on 2020/9/2.
//  Copyright © 2020 ZhouYou. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <YYModel/YYModel.h>


NS_ASSUME_NONNULL_BEGIN

@interface SBCModuleModel : NSObject<NSCoding,NSCopying>

@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) NSString *imageName;
@property (nonatomic , assign) BOOL selected;
@property (nonatomic, copy) NSString *jumpUrl;

@end

NS_ASSUME_NONNULL_END
