//
//  SBCCommonDefine.h
//  SBCSDK
//
//  Created by ZhouYou on 2019/12/26.
//  Copyright © 2019 ZhouYou. All rights reserved.
//

#ifndef SBCCommonDefine_h
#define SBCCommonDefine_h

//根据RGB值获取颜色
#define SBCColorFromRGB(rgbValue) [UIColor \
colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 \
green:((float)((rgbValue & 0xFF00) >> 8))/255.0 \
blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]

//根据RGB值和alpha值获取颜色
#define SBCCOLOR_RGB_ALPHA(rgbValue,i) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 \
green:((float)((rgbValue & 0xFF00) >> 8))/255.0 \
blue:((float)(rgbValue & 0xFF))/255.0 alpha:(i)]

//debug日志信息
#ifdef DEBUG
# define SBCLog(msg, ...) NSLog(@"[ %@:(%d)] %@<-- %@ -->",[[NSString stringWithUTF8String:__FILE__] lastPathComponent], __LINE__,[NSString stringWithUTF8String:__FUNCTION__], [NSString stringWithFormat:(msg), ##__VA_ARGS__]);
#else
# define SBCLog(...);
#endif


//weak self
#define SBCWS(weakSelf)  __weak __typeof(&*self)weakSelf = self

#define SBCSCREEN_RECT     [UIScreen mainScreen].bounds
#define SBCSCREEN_WITH     [UIScreen mainScreen].bounds.size.width
#define SBCSCREEN_HEIGHT   [UIScreen mainScreen].bounds.size.height

#define SBCFORMAT(fmt,...)[NSString stringWithFormat:fmt,##__VA_ARGS__]

#define SBCBundle(bundle)    [NSBundle bundleWithPath:[[NSBundle mainBundle] pathForResource:(bundle) ofType:@"bundle"]]

#endif /* SBCCommonDefine_h */
