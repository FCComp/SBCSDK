//
//  SBCBaseHttpManager.h
//  SBCSDK
//
//  Created by ZhouYou on 2020/1/8.
//  Copyright © 2020 ZhouYou. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@class SBCBaseHttp;

@interface SBCBaseHttpManagerModel : NSObject

@property (nonatomic, strong) NSString *vc;
@property (nonatomic, strong) SBCBaseHttp *request;

@end


@interface SBCBaseHttpManager : NSObject

@property (nonatomic, strong) NSMutableArray *requestModels;

+ (SBCBaseHttpManager *)instance;

- (void)addRequest:(SBCBaseHttp *)request;

- (void)removeRequest:(SBCBaseHttp *)request;

- (void)cancelAllRequestWithVc:(NSString *)vc;

@end

NS_ASSUME_NONNULL_END
