//
//  SBCBaseHttp.h
//  SBCSDK
//
//  Created by ZhouYou on 2019/12/26.
//  Copyright © 2019 ZhouYou. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SBCBaseHttpSSLConfig.h"



typedef enum : NSUInteger {
    SBCRequestGet,
    SBCRequestPost,
    SBCRequestDelete,
    SBCRequestPut,
} SBCRequestType;//请求类型

typedef enum : NSUInteger {
    SBCRequestSerializerTypeJson,
    SBCRequestSerializerTypeHttp,
} SBCRequestSerializerType;//requestSerializer

typedef enum : NSUInteger {
    SBCResponseSerializerTypeJson,
    SBCResponseSerializerTypeHttp,
    SBCResponseSerializerTypeXml,
} SBCResponseSerializerType;//responseSerializer

typedef void(^SBCRequestComplete)(id _Nullable data,  NSString * _Nullable error);

@interface SBCBaseHttp : NSObject

@property (nonatomic, copy) NSString *baseUrl;//请求域名，比如：http://sbc.sobeylingyun.com

@property (nonatomic, strong, readonly) NSURLSessionTask *requestTask;

@property (nonatomic, copy, nullable) SBCRequestComplete resultBlock;//请求结果回调

@property (nonatomic, strong) id param;//请求参数

@property (nonatomic, assign) SBCRequestType requestType;//请求类型，get,post

@property (nonatomic, assign) NSTimeInterval requestTimeoutInterval;//超时时间

@property (nonatomic, assign) SBCRequestSerializerType requestSerializerType;//默认SBCRequestSerializerTypeJson

@property (nonatomic, assign) SBCResponseSerializerType responseSerializerType;//默认SBCResponseSerializerTypeJson

@property (nonatomic, copy) NSString *requestURLPath;//请求接口地址，比如/sbc/api/auth/login

@property (nonatomic, strong) NSDictionary<NSString *, NSString *> * requestHeaderFieldValueDictionary;//header参数

@property (nonatomic, strong) SBCBaseHttpSSLConfig *sslConfig;//ssl配置

- (void)startRequest;//开始请求

+ (SBCBaseHttp *)instanceWithPostUrl:(NSString *)url
                             baseUrl:(NSString *)baseUrl
                               param:(nullable id)param
                              result:(SBCRequestComplete)resultBlock;

+ (SBCBaseHttp *)instanceWithGetUrl:(NSString *)url
                            baseUrl:(NSString *)baseUrl
                              param:(nullable id)param
                             result:(SBCRequestComplete)resultBlock;

+ (void)postWithUrl:(NSString *)url
            baseUrl:(NSString *)baseUrl
              param:(nullable id)param
             result:(SBCRequestComplete)resultBlock;

+ (void)getWithUrl:(NSString *)url
           baseUrl:(NSString *)baseUrl
             param:(nullable id)param
            result:(SBCRequestComplete)resultBlock;

+ (SBCBaseHttp *)instanceWithDeleteUrl:(NSString *)url baseUrl:(NSString *)baseUrl param:(id)param result:(SBCRequestComplete)resultBlock;

+ (void)deleteWithUrl:(NSString *)url baseUrl:(NSString *)baseUrl param:(id)param result:(SBCRequestComplete)resultBlock;

+ (SBCBaseHttp *)instanceWithPutUrl:(NSString *)url baseUrl:(NSString *)baseUrl param:(id)param result:(SBCRequestComplete)resultBlock;

+ (void)putWithUrl:(NSString *)url baseUrl:(NSString *)baseUrl param:(id)param result:(SBCRequestComplete)resultBlock;

@end


