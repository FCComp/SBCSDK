//
//  SBCConfig.h
//  SBCSDK
//
//  Created by ZhouYou on 2019/12/26.
//  Copyright © 2019 ZhouYou. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@class UIColor;
@interface SBCConfig : NSObject

+ (SBCConfig *)instance;

/*主题颜色，默认wihteColor
一般会在导航组件加载配置后根据配置设定。（可能为空，导航组件加载后才有）
 */
@property (nonatomic, strong) UIColor *themeColor;

/**
 导航栏tintColor颜色，即文字等颜色
 */
@property (nonatomic, strong) UIColor *navigationTintColor;

/**
 导航控制器背景颜色值类型，0：16进制色值字符串（如1479D7），1：图片base64字符串，2：图片网络链接,默认为色值字符串
 */
@property (nonatomic, assign) NSInteger navigationBarBackgroundType;

/**
 导航控制器背景颜色值,值类型参照navigationBarBackgroundType字段
 */
@property (nonatomic, strong) NSString *navigationBarBackgroundValue;

/*域名，http://或者https://开头，+ip+端口
 一般会在导航组件加载配置后根据配置设定
 */
@property (nonatomic, copy) NSString *domain;


/*JPush推送相关key，框架使用
 */
@property (nonatomic, copy) NSString *jpushAppkey;
@property (nonatomic, copy) NSString *jpushChannel;

@property (nonatomic, copy) NSString *sbc_ltc_domain;//ltc接口域名
@property (nonatomic, copy) NSString *sbc_ltc_h5;//ltc网页域名
@property (nonatomic, copy) NSString *sbc_ltc_caculate;//LTC计算器
@property (nonatomic, copy) NSString *sbc_k_caculate;//K计算器
@property (nonatomic, copy) NSString *app_ar_for_mbu;//AR-专业媒体
@property (nonatomic, copy) NSString *app_ar_in_pbu;//泛媒体AR
@property (nonatomic, copy) NSString *app_ar_channel;//AR-渠道部
@property (nonatomic, copy) NSString *app_ar_for_pbu;//AR-泛媒体

/*
 根据key取各个组件在“SBCConfig.plist”中自定义的配置数据
*/
 - (NSDictionary *)featureByName:(NSString *)key;

//@property (nonatomic, copy) NSDictionary *serversConfig;

@end

NS_ASSUME_NONNULL_END
