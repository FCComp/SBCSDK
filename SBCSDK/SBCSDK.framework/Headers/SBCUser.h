//
//  SBCUser.h
//  SBCSDK
//
//  Created by ZhouYou on 2019/12/26.
//  Copyright © 2019 ZhouYou. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SBCModuleModel.h"
#import "SBCUserCommonDefine.h"

NS_ASSUME_NONNULL_BEGIN

@interface SBCUser : NSObject


@property (nonatomic, copy) NSString *token;//登陆后的token
@property (nonatomic, copy) NSString *fc_token;//峰潮token
@property (nonatomic, copy) NSString *fc_h5_url;//峰潮跳转URL


@property (nonatomic, assign) int id;
@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) NSString *mobile;
@property (nonatomic, copy) NSString *email;
@property (nonatomic, copy) NSString *head_img;

@property (nonatomic, assign) int status;
@property (nonatomic, assign) int wallet_status;

@property (nonatomic, assign) int type;
@property (nonatomic, assign) int create_user_id;
@property (nonatomic, assign) int department_id;

@property (nonatomic, copy) NSString *create_time;
@property (nonatomic, copy) NSString *update_time;
@property (nonatomic, copy) NSString *status_name;
@property (nonatomic, copy) NSString *wallet_status_name;
@property (nonatomic, copy) NSString *work_time;
@property (nonatomic, copy) NSString *entry_date;
@property (nonatomic, copy) NSString *exchange_points;
@property (nonatomic, copy) NSString *format_entry_date;
@property (nonatomic, copy) NSString *freeze_points;
@property (nonatomic, copy) NSString *points;
@property (nonatomic, copy) NSString *position;
/**
  role = staff 员工
  role = leader 领导
 */
@property (nonatomic, copy) NSString *role;
@property (nonatomic, copy) NSString *role_name;
/**
  t_create = Y 有T合约创建权限/N 没有
  t_check = Y 有T合约评审权限/N 没有
 */
@property (nonatomic, copy) NSString *t_create;
@property (nonatomic, copy) NSString *t_check;

@property (nonatomic, copy) NSString *ltc_token;//ltc平台token
@property (nonatomic, copy) NSString *chain_token;//积分私钥
/**
 
 "app_subscribe", = 排班管理
 "app_user_info", = 人员信息
 "app_project" = 项目概览
  存在就代表有权限
 */
@property (nonatomic, strong) NSArray *workantAccess;//工单系统权限列表
@property (nonatomic, assign) NSInteger app_subscribe;//排班管理权限 1=有权限，0 = 没有权限
@property (nonatomic, assign) NSInteger app_user_info;//人员信息 1=有权限，0 = 没有权限
@property (nonatomic, assign) NSInteger app_project;//项目概览 1=有权限，0 = 没有权限
@property (nonatomic, assign) NSInteger notice_count;//未读的通知个数

@property (nonatomic, copy) NSString *app_ar_for_mbu;//AR-专业媒体
@property (nonatomic, copy) NSString *app_ar_in_pbu;//泛媒体AR
@property (nonatomic, copy) NSString *app_ar_channel;//AR-渠道部
@property (nonatomic, copy) NSString *app_ar_for_pbu;//AR-泛媒体



@property (nonatomic , strong) NSMutableArray<SBCModuleModel *> *defult_module_list;//全部APP列表

@property (nonatomic , strong) NSMutableArray<SBCModuleModel *> *selected_module_list;//已经选中的APP列表

+ (SBCUser *)instance;


//销毁数据
+ (void)terminateInstance;

//持久化数据
+ (void)persist;

//设置user
- (void)setUserInfo:(NSDictionary *)sbcUser;
//设置极光推送别名
- (void)setUserJpushAlias:(NSString *)alias;
//删除极光推送别名
- (void)deleteUserJpushAlias;
//获取模块的跳转地址
- (NSString *)getJumpUrl:(NSString *)name;

@end

NS_ASSUME_NONNULL_END
