//
//  SBCViewController.h
//  SBCSDK
//
//  Created by ZhouYou on 2019/12/26.
//  Copyright © 2019 ZhouYou. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface SBCViewController : UIViewController

/**
 * 用于对Controller的参数传递，标准json格式等字符串
 */
@property (nonatomic, copy) NSString *paramStr;

/**
 * 对一个Native的Controller发送消息
 * message  消息内容，标准Json格式
 * controller  目标Controller
 * from 发送者，自由定义
 */
- (void)sendSBCMessage:(NSString *)message toController:(id)controller from:(NSString *)from;

/**
 * 对一个Native的Controller发送消息
 * message  消息内容，标准Json格式
 * className  目标Controller类名
 * from 发送者，自由定义
 */
- (void)sendSBCMessage:(NSString *)message toControllerClassName:(NSString *)className from:(NSString *)from;

/**
 * 收到消息的回调，需重写此方法
 * message  消息内容，标准json格式
 */
- (void)receivedSBCMessage:(NSDictionary *)message;

/**
 * 根据ParamStr自动转换成字典（若paramStr为标准json格式）
 */
- (NSDictionary *)paramDict;

/**
 是否允许横屏，默认为不允许横屏
 */
@property (nonatomic, assign) BOOL allowLandscape;

@end

NS_ASSUME_NONNULL_END
